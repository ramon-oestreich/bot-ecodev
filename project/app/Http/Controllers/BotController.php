<?php

namespace App\Http\Controllers;

use CodeBot\CallSendApi;
use CodeBot\Message\Text;
use CodeBot\SenderRequest;
use Illuminate\Http\Request;
use CodeBot\WebHook;

class BotController extends Controller
{
    public function subscribe()
    {
        $webHook = new WebHook();
        $subscribe = $webHook->check(config('botfb.validationToken'));

        if (!$subscribe) {
            abort(403, 'Unauthorized action');
        }
        return $subscribe;
    }

    public function receiveMessage(Request $request)
    {
        $sender =  new SenderRequest();
        $senderId = $sender->getSenderId();
        $message = $sender->getMessage();

        $text = new Text($senderId);
        $callSendApi = new CallSendApi(config('botfb.FB_PAGE_ACCESS_TOKEN'));
        $callSendApi->make($text->message('Oi eu sou o bot Kardrean e nao leio :) '));
        $callSendApi->make($text->message('Tu digitou: ' . $message));

        return '';
    }
}

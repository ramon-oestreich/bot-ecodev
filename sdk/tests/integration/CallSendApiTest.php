<?php

namespace CodeBot\Message;

use CodeBot\CallSendApi;
use PHPUnit\Framework\TestCase;
use CodeBot\Message\Text;

class CallSendApiTest extends TestCase
{
    /**
     * @expectedException \GuzzleHttp\Exception\ClientException
     */

    public function testMakeRequest()
    {

        $message = (new Text(1))->message('Opa');
        (new CallSendApi('sadsadsadsad'))->make($message);
    }
}

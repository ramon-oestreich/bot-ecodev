<?php

namespace CodeBot\Message;

use PHPUnit\Framework\TestCase;

class TextTest extends TestCase{

    public function testReturnAnArray(){
        $actual = (new Text(1))->message('Opa');
        $expected = [
            'recipient' => [
                'id' => 1
            ],
            'message' => [
                'text'  => 'Opa',
                'metadata'  => 'DEVELOPER_DEFINED_METADATA'
            ]
        ];
        $this->assertEquals($actual,$expected);
    }
}
